import * as types from '../constants';

export const updateLesson = (moduleId, lessonId) => ({
    type: types.UPDATE_LESSON,
    payload: {moduleId, lessonId}
});

export const setEditMode = moduleId => ({
    type: types.SET_EDIT_MODE,
    payload: moduleId
});

export const archiveModule = moduleId => ({
    type: types.ARCHIVE_MODULE,
    payload: moduleId
});

export const getModules = _ => ({
    type: types.GET_MODULES,
    payload: null
});

export const addModule = module => ({
    type: types.ADD_MODULE,
    payload: module
});

export const updateModule = (id, data) => ({
    type: types.UPDATE_MODULE,
    payload: {id, data}
});

export const deleteModule = moduleId => ({
    type: types.DELETE_MODULE,
    payload: moduleId
});