import React from 'react';
import { connect } from 'react-redux';
import {addModule} from '../actions';
import ModuleForm from '../components/ModuleForm';

const ModuleCreator = ({addModule}) => {
    return (
        <div className="module-creator">
            <ModuleForm addModule={addModule} />
        </div>
    )
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
    addModule: paylaod => dispatch(addModule(paylaod))
});

export default connect(mapStateToProps, mapDispatchToProps)(ModuleCreator);