import React from 'react';
import { connect } from 'react-redux';

const Progress = ({progressData}) => {
    return (
        <div className="progress">
            <span>Votre progression</span>
            <div className="progress-container">
                <div className="progress-bar" style={{width: `${progressData.percent}%`}}>
                    {progressData.percent > 0 && <span className="progress-number">{`${progressData.percent} %`}</span>}
                </div>
            </div>
            <span className="progress-names">
                {progressData.names.join(', ')}
            </span>
        </div>
    )
}

const getPercent = (modules) => {
    const modulesFinished = modules.filter(module => {
        return module.lessons.length && module.lessons.every(lesson => lesson.done);
    });

    const names = modulesFinished.reduce((names, module) => {
        names.push(module.name);
        return names;
    }, []);

    const percent = parseInt((modulesFinished.length * 100 / modules.length), 10);

    return {percent, names};
}

const mapStateToProps = ({moduleReducer}) => ({
    progressData: getPercent(moduleReducer)
});

export default connect(mapStateToProps)(Progress);