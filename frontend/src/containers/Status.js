import React from 'react';
import { connect } from 'react-redux';

const Status = ({archived, published}) => {
    return (
        <span className="status">{archived} archivé(s) / {published} publié(s)</span>
    )
}

const count = (modules, filter) => {
    return modules.reduce(
        (acc, m) => acc += (m.archive ? m.archive === filter : filter === false) ? 1 : 0
    , 0);
}

const mapStateToProps = ({moduleReducer}) => ({
    archived: count(moduleReducer, true),
    published: count(moduleReducer, false)
});

export default connect(mapStateToProps)(Status);