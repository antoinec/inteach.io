import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import * as ActionCreators from '../actions';
import {ARCHIVED, UNARCHIVED} from '../constants';
import List from '../components/List';

class Listing extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: null
        }

        this.renderContent = this.renderContent.bind(this);

        this.boundActionCreators = bindActionCreators(ActionCreators, this.props.dispatch);
    }

    componentWillMount() {
        this.boundActionCreators.getModules();
    }

    renderContent() {
        const {modules} = this.props;

        let content = <p>Chargement ...</p>;

        if(modules && modules.length) {
        
            content = <List 
                        data={modules}
                        {...this.boundActionCreators} />
        }

        return content;
    }

    render() {
        return (
            <Fragment>
                {this.renderContent()}
            </Fragment>
        )
    }
}

const getModulesFiltered = (modules, filter) => {
    switch(filter) {
        case ARCHIVED:
            return modules.filter(module => module.archive === filter);
        case UNARCHIVED:
            return modules.filter(module => module.archive === filter || !!module.archive === false);
        default:
            break;
    }
}

const mapStateToProps = ({moduleReducer, archiveReducer}) => ({
    modules: getModulesFiltered(moduleReducer, archiveReducer)
});

export default connect(mapStateToProps)(Listing);