const data = require('./modules.json');

const getData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(_ => resolve(data), 500);
    });
}

export default getData;