import React from 'react';
import Listing from './containers/Listing';
import Status from './containers/Status';
import Progress from './containers/Progress';
import ModuleCreator from './containers/ModuleCreator';
import './index.css';
import 'milligram/dist/milligram.min.css';

const App = () => (
  <div className="container">
    <div className="row center">
      <h1>InTeach.io</h1>
      <Status />
    </div>
    <ModuleCreator />
    <Progress />
    <Listing />
  </div>
)

export default App;
