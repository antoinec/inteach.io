import React, {Component, Fragment} from 'react';
import Actions from './Actions';

class Module extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: this.props.data.name,
            description: this.props.data.description
        }

        this.renderInput = this.renderInput.bind(this);
    }

    renderInput(prop, key, className) {
        if(this.props.data.edit) {
            return <input onChange={({target}) => this.setState({[key]: target.value})} value={prop} />
        }

        return <span className={`module-entry ${className}`}>{prop}</span>;
    }

    render() {

        const {name, description} = this.state;
        const {id, edit, archive} = this.props.data;

        return (
            <Fragment>
                {this.renderInput(name, 'name', 'module-name')}
                {this.renderInput(description, 'description', 'module-desc')}

                <Actions
                    isEditing={edit}
                    isArchived={archive}
                    setEditMode={() => this.props.setEditMode(id)}
                    updateModule={() => this.props.updateModule(id, {name, description})}
                    archiveModule={() => this.props.archiveModule(id)}
                />
            </Fragment>
        )
    }
}

export default Module;