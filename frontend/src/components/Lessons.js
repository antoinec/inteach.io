import React from 'react';

const Lessons = ({id, data, updateLesson}) => {
    return (
        <div className="module-lessons">
            <span className="module-entry">{data && data.length} leçon(s)</span>

            <div className="lessons-list">
                <ul>
                    {data.map(lesson => {
                        return (
                            <li className={lesson.done ? 'lesson-done' : ''} key={lesson.name}>
                                <label>
                                    {lesson.name} : <span className="lesson-level">{lesson.level}</span>
                                    <input type="checkbox" onChange={() => {updateLesson(id, lesson.id)}} />
                                </label>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}

export default Lessons;