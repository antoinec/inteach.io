import React, {Component, Fragment} from 'react';
import {uniq} from '../utils';

class ModuleForm extends Component {
    constructor(props) {
        super(props);

        this.defaultLesson = {
            name: '',
            level: 'Beginner',
        }

        this.state = {
            name: '',
            description: '',
            lessons: [],
            tmpLesson: this.defaultLesson
        }

        this.mapLabels = {
            name: 'Nom',
            description: 'Description'
        }

        this.addLesson = this.addLesson.bind(this);
        this.displayLessonForm = this.displayLessonForm.bind(this);
        this.addModule = this.addModule.bind(this);
    }

    generateLabels() {
        return (
            ['name', 'description'].map((value) => {
                return (
                    <div className="label-container" key={value}>
                        <label htmlFor={value}>
                            {this.mapLabels[value]}
                        </label>
                        <input id={value} onChange={({target}) => this.setState({[value]: target.value})} value={this.state[value]} />
                    </div>
                )
            })
        )
    }

    addLesson() {

        if(!this.state.tmpLesson.name.length) {
            return;
        }

        this.setState({
            lessons: this.state.lessons.concat({...this.state.tmpLesson, id: uniq()}),
            tmpLesson: this.defaultLesson
        });
    }

    displayLessonForm() {

        const {tmpLesson} = this.state;

        return (
            <Fragment>
                <div>
                    <label>Nom</label>
                    <input onChange={({target}) => {
                        this.setState({tmpLesson: {...tmpLesson, name: target.value}})
                    }} value={tmpLesson.name} />
                </div>

                <div>
                    <label>Niveau</label>
                    
                    <select onChange={({target}) => {

                        this.setState({tmpLesson: {...tmpLesson, level: target.value}})
                        
                    }} defaultValue={tmpLesson.level}>
                    
                        {['Beginner', 'Intermediate', 'Harcore'].map(level => {
                            return <option key={level}>{level}</option>
                        })}
                    </select>
                </div>

                <button className="add-lesson" onClick={this.addLesson}>Ajouter la leçon</button>
            </Fragment>
        )

    }
    
    addModule() {
        const {tmpLesson, ...module} = this.state;
        
        this.props.addModule(module);

        this.setState({name: '', description: ''});
    }

    render() {

        const {lessons} = this.state;

        return (
            <form onSubmit={e => e.preventDefault()}>
                <fieldset>

                    <div className="labels">
                        {this.generateLabels()}
                    </div>

                    <div className="lessons-container">

                    <p>Leçons</p>

                    <div className="lessons-list lessons-list--small">
                        <ul>
                        {lessons.map(lesson => {
                            return (
                                <li key={lesson.name}>
                                    <label>
                                        {lesson.name} : <span className="lesson-level">{lesson.level}</span>
                                    </label>
                                </li>
                            )
                        })}
                        </ul>
                    </div>

                    {this.displayLessonForm()}

                    </div>

                </fieldset>

                <div className="add-container"> 
                    <button onClick={this.addModule}>Ajouter le module</button>
                </div>
            </form>
        )
    }
}

export default ModuleForm;