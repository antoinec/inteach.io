import React from 'react';

const Actions = ({isEditing, isArchived, setEditMode, updateModule, archiveModule}) => {
    return (
        <div className="actions">
            {isEditing && <button onClick={() => updateModule()}>Valider</button>}
            {!isEditing && <button onClick={() => setEditMode()}>Éditer</button>}
            {!isArchived && <button className="button-archive" onClick={() => archiveModule()}>Archiver</button>}
        </div>
    )
}

export default Actions;