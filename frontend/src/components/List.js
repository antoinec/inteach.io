import React from 'react';

import Module from './Module';
import Lessons from './Lessons';

const List = ({data, ...actionCreators}) => (
    <div className="row justify-between">
        {data.map(data => {
            return (
                <div className="module column" key={data.id}>
                    <Module 
                        data={data}
                        {...actionCreators}
                    />

                    <Lessons
                        id={data.id}
                        data={data.lessons}
                        {...actionCreators}
                    />
                </div>
            )
        })}
    </div>
)

export default List;