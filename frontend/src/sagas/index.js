import { call, put, takeLatest } from 'redux-saga/effects';
import fetchData from '../fixtures/api';
import * as types from '../constants';

function* getModulesAsync() {
    try {
        const data = yield call(fetchData);
        yield put({type: types.HTTP_SUCCESS, payload: data});
    } catch(e) {
        yield put({type: types.HTTP_ERROR, message: e.getMessage()});
    }
}

export function* getModules() {
    yield takeLatest(types.GET_MODULES, getModulesAsync);
}