import * as types from '../constants';

const archiveReducer = (state = types.UNARCHIVED, action) => {;
    switch(action.type) {
        case types.UNARCHIVED:
        case types.ARCHIVED:
            return action.filter;
        default:
            return state;
    }
}

export default archiveReducer;