import { combineReducers } from 'redux'
import archiveReducer from './archive';
import * as types from '../constants';
import {uniq} from '../utils';

const typePropertyMapping = {
    [types.SET_EDIT_MODE]: 'edit',
    [types.ARCHIVE_MODULE]: 'archive'
};

const toggleLesson = (state, {moduleId, lessonId}) => {
    const module = state.find(m => m.id === moduleId);
    module.lessons = module.lessons.map(lesson => {
        if(lesson.id === lessonId) {
            lesson = {...lesson, done: !(!!lesson.done)};
        }
        return lesson;
    });

    return state.map(m => m.id === moduleId ? {...m, ...module} : m);
};

const moduleReducer = (state = [], action) => {
    switch(action.type) {
        case types.HTTP_SUCCESS:
            return [].concat(action.payload);
        case types.UPDATE_MODULE:
            return state.map(module => {
                if(module.id === action.payload.id) {
                    module = {...module, edit: false, ...action.payload.data}
                }

                return module;
            })
        case types.SET_EDIT_MODE:
        case types.ARCHIVE_MODULE:
            return state.map(module => {
                if(module.id === action.payload) {
                    module[typePropertyMapping[action.type]] = true;
                }

                return module;
            });
        case types.UPDATE_LESSON:
            return toggleLesson(state, action.payload);
        case types.ADD_MODULE:
            return state.concat({...action.payload, id: uniq()}).reverse(); // Just to see the added one on top of the list
        default:
            return state;
    }
}

export default combineReducers({moduleReducer, archiveReducer});