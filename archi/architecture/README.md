# Plateforme SaaS

## Architecture en micro-services

J'ai choisi de m'orienter vers une architecture en serverless avec les services AWS en utilisant : 

 - L'API Gateway
 - Les Lambda (code)
 - Amazon S3 (storage files)
 - Amazon DynamoDB (storage NoSQL)
 - ElasticSearch (recherche, indexation de contenu)

Les services Amazon gèrent automatiquement le scaling ce qui réponds à ma problématique de fort traffic. De plus utiliser les aws me permet d'avoir des services performants, stables et à faible coût.

**API Gateway** est là pour ma gestion d'API, combinée avec les lambdas cela me permet de gérer mes API simplement. 
**Les Lambdas** vont me permettre de me concentrer uniquement sur mon code et d'oublier la partie serveur car tout est géré côté Amazon. Le coût est fixé en fonction de l'utilisation des fonctions et le scale est fait automatiquement si besoin.
**Amazon S3** pour le storage de fichiers, ici aussi l'avantage est son faible coût, sa haute disponibilité.
**AppSync / DynamoDB** n'ayant pas de préférences particulières pour le NoSQL par rapport au BDD à schéma relationnel j'ai choisi DynamoDB pour son intégration avec AppSync (qui permet de générer des resolvers GraphQL à partir de tables DynamoDB) mais aussi pour la compatibilité et le bon fonctionnement des services Amazon entre eux. L'avantage cependant est le "Point-in-Time Recovery" qui offre une grande souplesse et sécurité quant à la restauration de données en cas d'erreurs sur la base (suppression d'une table par exemple). 
**ElasticSearch** qui me parait adapté, au contexte étant donné que l'on est sur un site avec possiblement beaucoup de contenu, pour la recherche de texte.


## Backend

Avec l'architecture ci-dessus le backend est plutôt réduit (pas d'utilisation d'un framework comme Express pour le routing par exemple). Cependant j'opte pour l'utilisation de GraphQL plutôt que de REST pour les appels API. 
L'avantage, comme une application mobile est aussi prévue, est d'avoir un contrôle des données que l'on souhaite récupérer grâce aux Query ce qui permet de limiter les données à envoyer / récupérer sur le réseau, dans le cas d'application mobile où les personnes ne sont pas toujours en 4G c'est un gain non négligeable. À cela s'ajoute le fait qu'on a pas besoin d'effectuer de versionning d'API avec GraphQL.
La parfaite intégration de DynamoDB et AppSync (= gestion du GraphQL) permet aussi un gain de temps.
Pour finir l'utilisation de GraphQL permet de se passer d'un couple avec Redux pour le state management de notre Frontend.

Concernant la sécurité, tout serait en SSL avec utilisation des JWT Token pour l'authentifcation des requêtes à l'API.

## Frontend / Mobile

Concernant cette partie j’opterai pour la stack suivante :

 - React (Frontend)
 - React Native (Mobile)
 - Styled components
 - AWS AppSync React

L'utilisation de React et des Styled Components permet de partager un maximum de code entre l'application Front et Mobile.
AWS AppSync me permet de réduire le "boilerplate" de Redux en combinant directement les Queries et Mutation GraphQL dans mes components. À cela s'ajoute l'avantage de la gestion du offline, disponible par défaut (dans le cas de notre application mobile c'est parfait), de la gestion du cache, de l'optimistic UI (très utile lors de l'offline ou de réseau lent) ainsi que le temps réel avec les "Subscription".

**Conclusion**

Voici comment je m'orienterai si je devais construire une plateforme SaaS telle que décrite dans le document. N'étant pas un AdminSys j'ai volontairement opté pour l'abstraction de la gestion serveur pour me focaliser sur le code. Je n'ai pas détaillé la stack de développement (qui serait surement à base de Docker surtout si on passe par une architecture "classique" plutôt que l'utilisation des services AWS).

Si l'utilisation des services AWS n'était pas faisable ou possible, en quelques mots j'utiliserai les clusters de Node pour profiter au maximum des ressources serveur, à cela j'ajouterai un cache Redis qui permettra de distribuer les data entre les différents cluster (si un client est dirigé vers le cluster A et la fois d'après sur le B il faut qu'il ait accès aux mêmes données).

Pour gérer le scaling ainsi que le monitoring j'opterai pour PM2 qui gère les clusters, les montées de version lors de mise en production (comme un capistrano), le reload de cluser sans downtime.

Concernant la base de données j'utiliserai MongoDB pour sa bonne intégration dans l'éco-système Node mais aussi pour sa fonction "Replica Set" qui permet d'avoir des répliquas de la base de données principale et ainsi basculer entre les répliquas en cas de crash de la BDD principale.

La stack Frontend / Mobile resterai inchangée.
